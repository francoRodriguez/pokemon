import React, { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import CardPokemon from "./components/cardPokemon/CardPokemon";
import Footer from "./components/footer/Footer";
import Navbar from "./components/navBar/NavBar";
const Main = () => {
  const [allPokemons, setAllPokemons] = useState([]);
  const [loadMore, setLoadMore] = useState(
    "https://pokeapi.co/api/v2/pokemon?limit=18"
  );

  const getAllPokemons = async () => {
    const res = await fetch(loadMore);
    const data = await res.json();
    setLoadMore(data.next);

    function createPokemonObject(results) {
      results.forEach(async (pokemon) => {
        const res = await fetch(
          `https://pokeapi.co/api/v2/pokemon/${pokemon.name}`
        );
        const data = await res.json();
        setAllPokemons((currentList) => [...currentList, data]);
        await allPokemons.sort((a, b) => a.id - b.id);
      });
    }
    createPokemonObject(data.results);
    console.log(data.results);
  };

  useEffect(() => {
    getAllPokemons();
  }, []);

  return (
    <div className="App">
      <Navbar isPokedex={false} />
      <Container className="mt-4">
        <Row>
          <Col lg={12}>
            <Row xs={1} sm={2} md={3} xl={6} xxl={8} className="g-4">
              {allPokemons.map((pokemonStats) => (
                <CardPokemon pokemonStats={pokemonStats} />
              ))}
            </Row>
            <div className="text-center mt-3 mb-5">
              <button
                className="load-more btn btn-primary btn-pokemon"
                onClick={() => getAllPokemons()}
              >
                Cargar m&aacute;s Pok&eacute;mon
              </button>
            </div>
          </Col>
        </Row>
      </Container>
      <Footer />
    </div>
  );
};
export default Main;
