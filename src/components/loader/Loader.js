import React, { useState } from "react";
import { Card, Button, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

const Loader = () => {
  return <div class="o-pokeball c-loader u-bounce"></div>;
};

export default Loader;
