import React, { useState } from "react";
import { Card, Button, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

const CardPokemon = (data) => {
  var d = data.pokemonStats;
  var name = d.name.toUpperCase();
  var type = data.pokemonStats.types[0].type.name;
  var c = "mb-4 card-pokemon type-" + type;
  var urlTo = "pokedex?name=" + d.name;
  var id = d.id;
  var lengthID = id.toString().length;

  if (lengthID == 1) {
    id = "00" + id;
  } else if (lengthID == 2) {
    id = "0" + id;
  }
  var src =
    "https://assets.pokemon.com/assets/cms2/img/pokedex/full/" + id + ".png";
  return (
    <Col>
      <Link to={urlTo}>
        <Card className={c}>
          <Card.Img
            variant="top"
            src={src}
            //src={d.sprites.other.dream_world.front_default}
          />
          <Card.Body className="text-center">
            <Card.Subtitle>
              <b>{name}</b>
            </Card.Subtitle>
            <Card.Text>Nro: {d.order}</Card.Text>
          </Card.Body>
        </Card>
      </Link>
    </Col>
  );
};

export default CardPokemon;
