import React from "react";
import { Navbar, Container, Image } from "react-bootstrap";
import { Link } from "react-router-dom";

const Footer = (props) => {
  return (
    <div className="footer text-center">
      <Container>
        <Navbar.Brand href="/">
          <Image src="logo.png" className="logo-menu" />
        </Navbar.Brand>
      </Container>
    </div>
  );
};

export default Footer;
