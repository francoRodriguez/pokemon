import React from "react";
import { useState } from "react";
import {
  Navbar,
  Container,
  Nav,
  NavDropdown,
  Button,
  Image,
  Form,
  FormControl,
} from "react-bootstrap";
import { Link } from "react-router-dom";

const NavBar = (props) => {
  const [isPokedex, setisPokedex] = useState(props.isPokedex);
  var buttonsNavbar = (
    <Button variant="outline-warning" type="submit">
      <Link to="/pokedex" className="link-white">
        Pokedex
      </Link>
    </Button>
  );
  if (isPokedex) {
    buttonsNavbar = (
      <Button variant="outline-warning" type="submit">
        <Link to="/" className="link-white">
          Inicio
        </Link>
      </Button>
    );
  }
  return (
    <Navbar expand="lg" style={{ backgroundColor: "#ef5350", width: "100%" }}>
      <Container>
        <Navbar.Brand href="/">
          <Image src="logo.png" className="logo-menu" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">{buttonsNavbar}</Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBar;
