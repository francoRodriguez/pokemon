import React from "react";

const PersonalInformation = ({ key, type }) => {
  const style =
    "type-" + type + " card-poke-tipo list-group-item mt-1 text-uppercase";
  return (
    <React.Fragment>
      <li key={key} className={style}>
        {type}
      </li>
    </React.Fragment>
  );
};
export default PersonalInformation;
