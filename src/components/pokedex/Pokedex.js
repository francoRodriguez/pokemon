import React, { useState, useEffect } from "react";
import { Col, Image, Form, Button, Row, Alert } from "react-bootstrap";
import reactDom from "react-dom";
import InfoPokemon from "./InfoPokemon";
import ImgDefault from "./imgDefault.png";
import { useLocation } from "react-router-dom";
import Navbar from "../navBar/NavBar";
import Footer from "../footer/Footer";

const Pokedex = (props) => {
  const [name, setName] = useState([]);
  const [image, setImage] = useState(ImgDefault);
  const [backgroundColor, setBackgroundColor] = useState("card-poke-tipo");
  const [description, setDescription] = useState("");
  const [stats, setStats] = useState([]);
  const [search, setSearch] = useState("");
  const [personalInformation, setPersonalInformation] = useState([
    {
      height: "default",
      weight: "default",
      types: [],
    },
  ]);

  let query = new URLSearchParams(useLocation().search);

  const handleChange = (e) => {
    setSearch(e.target.value);
  };

  const handleClick = () => {
    findPokemon(search);
  };

  const showError = () => {
    const alert = document.getElementById("error");
    const infoPokemon = document.getElementById("infoPokemon");
    reactDom.findDOMNode(infoPokemon).classList.remove("show");
    reactDom.findDOMNode(infoPokemon).classList.add("hide");
    reactDom.findDOMNode(alert).classList.add("show");
  };

  const findDescription = async (url) => {
    const res = await fetch(url)
      .then((response) => {
        //console.log(response);
        if (response.ok) {
          return response.json();
          const alert = document.getElementById("error");
          reactDom.findDOMNode(alert).classList.remove("show");
        } else if (response.status === 404) {
          showError();
        } else {
          return Promise.reject("some other error: " + response.status);
        }
      })
      .then((data) => {
        let description = "";
        let countPhrases = 0;
        // recorro todas las descripciones
        data.flavor_text_entries.forEach((desc) => {
          // solo guardo la descripcion de los que estan en esp
          if (desc.language.name === "es" && countPhrases <= 3) {
            countPhrases++;
            description += desc.flavor_text + " ";
          }
        });
        setDescription(description);
      })
      .catch(function (error) {
        showError();
      });
  };

  const findPokemon = async (pokemonName) => {
    pokemonName = pokemonName.toLowerCase();
    const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + pokemonName)
      .then((response) => {
        //console.log(response);
        if (response.ok) {
          const infoPokemon = document.getElementById("infoPokemon");
          reactDom.findDOMNode(infoPokemon).classList.remove("hide");
          reactDom.findDOMNode(infoPokemon).classList.remove("show");
          const alert = document.getElementById("error");
          reactDom.findDOMNode(alert).classList.remove("show");
          return response.json();
        } else if (response.status === 404) {
          showError();
        } else {
          return Promise.reject("some other error: " + response.status);
        }
      })
      .then((data) => {
        console.log(data);
        var id = data.id;
        var lengthID = id.toString().length;

        if (lengthID === 1) {
          id = "00" + id;
        } else if (lengthID === 2) {
          id = "0" + id;
        }
        var srcImg =
          "https://assets.pokemon.com/assets/cms2/img/pokedex/full/" +
          id +
          ".png";

        // paso url de la descripcion
        findDescription(data.species.url);
        setName(data.name);
        setStats(data.stats);
        //setImage(data.sprites.other.dream_world.front_default);
        setImage(srcImg);
        //console.log(data.types[0].type.name);
        setBackgroundColor("type-" + data.types[0].type.name);
        let height = data.height / 10;
        let weight = data.weight / 10;
        setPersonalInformation([
          {
            height: height,
            weight: weight,
            types: data.types,
            ability: data.abilities[0].ability.name,
          },
        ]);
      })
      .catch(function (error) {
        showError();
      });
  };

  useEffect(() => {
    // saco el div de alert y infoPokemon al inicio
    const alert = document.getElementById("error");
    reactDom.findDOMNode(alert).classList.remove("show");

    let n = query.get("name");
    if (n != null) {
      setSearch(n);
      findPokemon(n);
    }
  }, []);

  return (
    <>
      <Navbar isPokedex={true} />
      <Col xl={12}>
        <Row>
          <Col xl={12} className="mt-9 center">
            <Form>
              <Form.Row className="align-items-center">
                <Col xl="auto">
                  <Form.Control
                    type="text"
                    placeholder=""
                    onChange={handleChange}
                  />
                </Col>
                <Col xl="auto">
                  <Button variant="success" onClick={handleClick}>
                    Buscar Pok&eacute;mon
                  </Button>
                </Col>
              </Form.Row>
            </Form>
          </Col>
        </Row>
        <Alert variant={"danger"} className="mt-3" id="error">
          El Pok&eacute;mon [{search}] no existe.
        </Alert>
        <div id="infoPokemon" className="info-pokemon hide">
          <InfoPokemon
            name={name}
            image={image}
            info={description}
            personalInformation={personalInformation}
            backgroundColor={backgroundColor}
            stats={stats}
          />
        </div>
      </Col>
      <Footer />
    </>
  );
};
export default Pokedex;
