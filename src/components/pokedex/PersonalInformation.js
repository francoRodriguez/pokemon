import React from "react";
import { Row, Col } from "react-bootstrap";
import TypePokemon from "./TypePokemon";

const PersonalInformation = ({
  height,
  category = "Raton",
  weight,
  ability,
  sex = "sex",
  types = [],
}) => (
  <React.Fragment>
    <div className="card-poke-default text-center">
      <Row>
        <Col xxl={6} className="card-info">
          <h3 className="card-poke-title">Altura</h3>
          <p className="card-poke-info">{height} m</p>
        </Col>
        <Col xxl={6} className="card-info">
          <h3 className="card-poke-title">Peso</h3>
          <p className="card-poke-info">{weight} kg</p>
        </Col>
      </Row>
    </div>
  </React.Fragment>
);

export default PersonalInformation;
