import React from "react";
import { Table, ProgressBar } from "react-bootstrap";

const Stats = (props) => {
  var { stats, backgroundColor } = props;
  //console.log(stats);
  var styleProgressBar = "background-color: " + backgroundColor;
  return (
    <React.Fragment>
      <h2>Estad&iacute;sticas</h2>
      <Table>
        <tbody>
          {stats.map((data) => (
            <tr>
              <td width="10%">{data.stat.name.toUpperCase()}</td>
              <td width="10%">{data.base_stat}</td>
              <td width="80%">
                <ProgressBar variant="warning" animated now={data.base_stat} />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </React.Fragment>
  );
};
export default Stats;
