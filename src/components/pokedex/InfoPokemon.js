import React from "react";
import { Col, Image, Table, Row, ProgressBar } from "react-bootstrap";
import PersonalInformation from "./PersonalInformation";
import TypePokemon from "./TypePokemon";
import { Container } from "react-bootstrap";
import Stats from "./Stats";

const InfoPokemon = (props) => {
  var { name, image, info, personalInformation, backgroundColor, stats } =
    props;
  var classImg = "img-pokemon " + backgroundColor;
  return (
    <React.Fragment>
      <Container>
        <Row>
          <Col xl={12}>
            <h1 className="text-capitalize text-center mb-5">{name}</h1>
          </Col>
        </Row>
        <Row>
          <Col xl={3} className="mb-5">
            <Container className="text-center">
              <Image src={image} className={classImg} fluid />
            </Container>
            <Container>
              <ul className="list-group mt-2 text-center">
                {personalInformation[0].types.map((t, key) => (
                  <TypePokemon key={key} type={t.type.name} />
                ))}
              </ul>
            </Container>
          </Col>
          <Col xl={9} className="mb-5">
            <h2>Descripci&oacute;n</h2>
            <p>{info}</p>
            <PersonalInformation
              height={personalInformation[0].height}
              weight={personalInformation[0].weight}
            />
          </Col>
        </Row>
        <hr />
        <Row>
          <Col xl={12}>
            <Stats stats={stats} backgroundColor={backgroundColor} />
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  );
};
export default InfoPokemon;
